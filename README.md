# blog

#### 介绍
Django简单的blog系统

基于python3.8和Django3.2的博客

页面布局使用Bootstrap v3.4.1版本

《Django实战：Python Web典型模块与项目开发》——张晓

这本书中的博客系统开发源码。

#### 源码修订
原书阅读及代码实现遇到的问题及修正

书中blog/views.py文件中定义的视图函数blogdetailview中缺少了get_queryset()的重写，项目运行时会报错。

`ImproperlyConfigured: blogdetailview is missing a QuerySet...`

在blogdetailview中增加了一下代码：

```
def get_queryset(self):
    return models.Blog.objects.all()
```

blogdetailview的完整代码如下：

```
class blogdetailview(DetailView):
    models = models.Blog
    template_name = 'blog/detail.html'
    context_object_name = 'blog'
    pk_url_kwarg = 'pk'

    def get_queryset(self):
        return models.Blog.objects.all()

    def get_object(self, queryset=None):
        blog = super(blogdetailview, self).get_object(queryset=None)
        blog.increase_views()
        return blog

    def get_context_data(self, **kwargs):
        context = super(blogdetailview, self).get_context_data(**kwargs)
        form = CommentForm()
        comment_list = self.object.comment_set.all()
        context.update({
            'form': form,
            'comment_list': comment_list
        })
        return context
```

书中是基于自带的sqlite数据库开发，迁移到mysql数据库之后运行文章归档功能会报错。

NoReverseMatch，提示解析url时缺少参数。

在sqlite中正常，切换到mysql就出错。

调试之后发现是归档功能函数获取不到数据，base.html文件中

```
<!-- 调用自定义标签文件custom_tags中定义的archives()函数，倒序显示有文章发表的年、月-->
{% archives as date_list %}
<ul>
    {% for date in date_list %}
    <li>
        <a href="{% url 'blog:archives' date.year date.month %}">{{ date.year }}年{{ date.month }}月</a>
    </li>
    {% empty %}暂无归档！{% endfor %}
</ul>
```

这部分date是没有数据的，所以date.year和date.month都没有数据

原因是mysql没有安装服务器时区支持

参考：https://dev.mysql.com/doc/refman/8.0/en/time-zone-support.html

windows系统下需要下载对应的文件，在mysql中加载

test_blog目录下的timezone_posix.sql文件

在终端执行命令

`mysql -u root -p mysql < timezone_posix.sql`

加载时区支持文件，然后重启mysql服务

启动项目，归档功能就正常了。

#### 主要功能

文章，页面，分类目录，标签的添加，删除，编辑等

用户的注册、登录、评论等。

#### 安装教程

1. 使用pip安装： `pip install -r requirements.txt`

2. 修改settings.py中的DATABASES数据库配置信息

3. 数据库迁移

   `python manage.py makemigrations`

   `python manage.py migrate`

4. 创建超级用户

5. `python manage.py createsuperuser`

6. 运行项目

   `python manage.py runserver`

#### 补充说明

1.  我是个新手，看书跑案例代码遇到一些问题，
2.  将问题的解决方法记下来，避免遗忘，同时也希望能帮到同样在学习的新手
