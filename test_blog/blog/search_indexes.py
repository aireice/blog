#!/user/bin/env python3
# -*- coding: utf-8 -*-
"""
__author__ = 'Karas'
__time__ = '2021/6/15 11:28'
"""
from haystack import indexes
from .models import Blog

class BlogIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        return Blog

    def index_queryset(self, using=None):
        return self.get_model().objects.all()