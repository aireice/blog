#!/user/bin/env python3
# -*- coding: utf-8 -*-
"""
__author__ = 'Karas'
__time__ = '2021/6/10 11:20'
"""
from django.urls import path, re_path
from . import views

app_name = 'blog'

urlpatterns = [
    path('test_ckeditor_front/', views.test_ckeditor_front),
    path('', views.indexview.as_view(), name='index'),
    path('registe/', views.registe, name='registe'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    re_path('blog/(?P<pk>[0-9]+)/', views.blogdetailview.as_view(), name='detail'),
    re_path('category/(?P<pk>[0-9]+)/', views.categoryview.as_view(), name='category'),
    re_path('tag/(?P<pk>[0-9]+)/', views.tagview.as_view(), name='tag'),
    re_path('archives/(?P<year>[0-9]{4})/(?P<month>[0-9]{1,2})/', views.archives, name='archives'),
    path('myindex/<int:loguserid>/', views.myindex.as_view(), name='myindex'),
    path('authorindex/<int:id>/', views.authorindex.as_view(), name='authorindex'),
]
