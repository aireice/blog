from django.contrib import admin
from .models import Blog, Category, Tag, loguser
# Register your models here.

class BlogAdmin(admin.ModelAdmin):
    list_display = ('title', 'created_time', 'modified_time', 'category', 'author', 'views', 'body',)

admin.site.register(loguser)
admin.site.register(Blog, BlogAdmin)
admin.site.register(Category)
admin.site.register(Tag)
