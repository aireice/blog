#!/user/bin/env python3
# -*- coding: utf-8 -*-
"""
__author__ = 'Karas'
__time__ = '2021/6/15 15:31'
"""
from django.urls import path
from . import views

app_name = 'comments'
urlpatterns = [
    path('comment/post/<int:blog_pk>/', views.blog_comment, name="blog_comment"),
]