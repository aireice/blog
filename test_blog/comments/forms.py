#!/user/bin/env python3
# -*- coding: utf-8 -*-
"""
__author__ = 'Karas'
__time__ = '2021/6/15 15:30'
"""
from django import forms
from .models import Comment

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text']